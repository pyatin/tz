<?php

return array(
    'file_upload' => array(
        'types' => 'xlsx,xls',
        'max_size' => 1048576, //  1 Mb
        'save_to_directory' => realpath(PUBLIC_PATH . DIRECTORY_SEPARATOR . 'uploads')
    )
);