<?php

return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'App Name',
    'defaultController' => 'default/index',

    'preload' => array('modulePreloader'),

    'modules' => array(
        'default'
    ),

    // application components
    'components' => array(
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'default/index/error',
        ),
        'urlManager' => array(
            'urlFormat' => 'path',
            'rules' => array(
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),
        ),
    ),
    'params' => require(dirname(__FILE__) . '/params.php'),
);