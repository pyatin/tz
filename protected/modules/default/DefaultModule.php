<?php

class DefaultModule extends CWebModule
{
	public function init()
	{

        $this->setImport(array(
            'default.models.forms.*'
		));

        $this->layoutPath = Yii::getPathOfAlias('default.views.layouts');
        $this->layout = 'default';

	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))	{
			return true;
		}
		else
			return false;
	}
}
