<?php

$form = $this->beginWidget('CActiveForm', array(
    'enableClientValidation' => false,
    'htmlOptions' => array(
        'enctype' => 'multipart/form-data',
    ),
));

/* @var $form CActiveForm */

?>

<?php echo $form->label($model, 'document'); ?>
<?php echo $form->fileField($model, 'document'); ?>
<?php echo $form->error($model, 'document'); ?>

<?php echo CHtml::submitButton('Upload'); ?>


<?php $this->endWidget(); ?>