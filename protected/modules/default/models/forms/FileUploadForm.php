<?php

class FileUploadForm extends CFormModel
{

    public $document;

    public function rules()
    {
        $params = Yii::app()->params->file_upload;

        return array(
            array('document', 'file', 'types' => $params['types'], 'maxSize' => $params['max_size']),
        );
    }

    public function attributeLabels()
    {
        return array(
            'document' => 'Document'
        );
    }

}