<?php

class IndexController extends CController
{

    public function actionIndex()
    {
        $request = Yii::app()->request;

        $model = new FileUploadForm();

        if ($request->isPostRequest && $model->validate()) {
            $uploadedFile = CUploadedFile::getInstance($model, 'document');
            $params = Yii::app()->params->file_upload;

            $fileName = time() . '.' . pathinfo($uploadedFile->getName(), PATHINFO_EXTENSION);

            $filePath = $params['save_to_directory'] . DIRECTORY_SEPARATOR . $fileName;
            $uploadedFile->saveAs($filePath);
            @chmod($filePath, 0775);
            $this->redirect(Yii::app()->urlManager->createUrl('default/index/viewFile', array(
                'file' => $fileName
            )));
        }

        $this->render('index', array(
            'model' => $model
        ));
    }

    public function actionViewFile()
    {
        $fileName = (string) Yii::app()->request->getParam('file');
        $params = Yii::app()->params->file_upload;

        $pathinfo = pathinfo($fileName);

        $filePath = $params['save_to_directory'] . DIRECTORY_SEPARATOR . $fileName;

        $invalidExt = !in_array($pathinfo['extension'], explode(',', $params['types']));
        $invalidDir = $pathinfo['dirname'] !== '.';
        $notExist = !is_file($filePath);

        if($invalidExt || $invalidDir || $notExist) {
            throw new CHttpException(404);
        }

        Yii::import('ext.yiiexcel.YiiExcel', true);
        Yii::registerAutoloader(array('YiiExcel', 'autoload'), true);

        $objPHPExcel = PHPExcel_IOFactory::load($filePath);

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'HTML');
        /* @var $objWriter PHPExcel_Writer_HTML */

        $this->render('view', array(
            'css' => $objWriter->generateStyles(),
            'html' => $objWriter->generateSheetData()
        ));
    }

    public function actionError()
    {
        $error = Yii::app()->errorHandler->error;

        if (is_array($error)) {
            $this->render('error', $error);
        }
    }

}